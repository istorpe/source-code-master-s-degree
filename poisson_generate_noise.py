from dolfin import *
import random

noise_list = [0.003, 0.01, 0.03, 0.1]
N_list = [4, 8, 16, 32, 64, 128]
for N in N_list:
	print "Creating noise, N=%i" % N
	mesh = UnitSquareMesh(N, N)
	V = FunctionSpace(mesh, "CG", 1)
	for noise in noise_list:
		u_exact = Expression('exp(-k*x[0])', k = 2.0, degree = 1)
		u_exact_V = project(u_exact, V)

		u_MR_mesh = MeshFunction("double", mesh, 2)

		for cell in cells(mesh):
			u_MR_mesh[cell] = random.gauss(0, noise)*norm(u_exact_V, 'L2')

		u_MR_mesh_file = File("noise/u_MR_mesh_noise_%s_N_%i.xml.gz"% (noise, N))
		u_MR_mesh_file << u_MR_mesh

		plot(mesh, title="mesh")
		plot(u_MR_mesh, title="noise_%s_N_%i" % (noise, N))
