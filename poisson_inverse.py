from dolfin import *
import ufl, sys, random, numpy
from time import clock, strftime, gmtime

"""
Solves the minimization problem
	||u - u_MR||**2 + alpha*||mu - mu_prior||**2 --> min!
subject to
    u(x, y) - div (mu grad u(x, y)) = 0 		in Omega
with 
   						   u(x, y)  = exp(-k*x) on Gamma_left
    					   mu(x, y) = 1/k^2     on Gamma_right

Usage:   inverse_poisson.py convergence_rates makeplot test_alpha write_to_file
Example: inverse_poisson.py True False True True

Options: 
convergence_rates:  "True", "False"
makeplot:           "True", "False"
test_alpha:         "True", "False"
write_to_file:      "True", "False"
"""

def convergence_rate(E, h):
    from math import log as ln
    r = []
    for i in range(1, len(E)):
        if E[i] and E[i-1]:
            r.append(ln(E[i]/E[i-1])/ln(h[i]/h[i-1]))
        else:
            r.append("False")
    return r

def solver(N, k, noise, degree=1, makeplot=False, alpha=0.0, mu_space="constant", mu_prior_function="0"):
    # Create mesh 
    mesh = UnitSquareMesh(N, N)

    # Create classes for defining parts of the boundaries
    class Left(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[0], 0.0)
    class Right(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[0], 1.0)
    class Bottom(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], 0.0)
    class Top(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], 1.0)

    # Initialize sub-domain instances
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()

    V = FunctionSpace(mesh, 'Lagrange', degree)
    if mu_space == "constant":
        S = FunctionSpace(mesh, 'R', 0)
    elif mu_space == "DG":
        S = FunctionSpace(mesh, 'DG', 0)

    M = MixedFunctionSpace([V, S, V])

    # Code for C++ evaluation of noise function
    noise_code = """
    class Noise : public Expression
    {
    public:
      // Create expression
      Noise() : Expression() {}

      // Function for evaluating expression on each cell
      void eval(Array<double>& values, const Array<double>& x, const ufc::cell& cell) const
      {
        const uint D = cell.topological_dimension;
        const uint cell_index = cell.index;
        values[0] = (*noise_mesh)[cell_index];
      }

      // The data stored in mesh functions
      boost::shared_ptr<MeshFunction<double> > noise_mesh;
    };
    """

    # Define known functions
    u_exact = Expression('exp(-k*x[0])', k = k, degree = degree)
    mu_exact = Expression('1.0/(k*k)', k = k, degree = degree)

    if noise != 0.0:
        # Define noise expression 
        noise_mesh = MeshFunction("double", mesh, "noise/u_MR_mesh_noise_%s_N_%i.xml.gz" % (noise, N))
        noise_function = Expression(cppcode=noise_code)
        noise_function.noise_mesh = noise_mesh
        u_MR = u_exact + noise_function
    else:
        u_MR = u_exact
    
    if mu_prior_function == "0":
        mu_prior = Constant(0.0)
    elif mu_prior_function == "mu_exact":
        mu_prior = mu_exact

    print "----------------------------------------------------------------------------------"
    print "Computing solution. mu_prior = %s,  alpha = %s, noise = %s, N = %i, degree = %i \n" \
            % (mu_prior_function, alpha, noise, N, degree)

    # Define trial functions and test functions 
    z = TestFunction(M)
    (v, eta, q) = split(z)

    # Define function
    y = Function(M)

    # Set initial guesses
    u0  = u_exact
    mu0 = mu_exact
    w0  = Expression("1.0")

    y0 = project(as_vector((u0, mu0, w0)), M)
    y.assign(y0)
    
    # Split mixed functions
    (u, mu, w) = split(y)

    # Initialize mesh function for boundary domains
    boundaries = FacetFunction("size_t", mesh)
    left.mark(boundaries, 0)
    top.mark(boundaries, 2)
    right.mark(boundaries, 1)
    bottom.mark(boundaries, 2)

    # Define boundary conditions
    t_l = Constant(1.0/k)
    t_r = Constant(-exp(-k)/k)
    n = FacetNormal(mesh)
    t = dot(project(mu_exact, S)*grad(project(u_exact, V)), n)

    # Define new measures associated with the boundaries
    ds = Measure("ds")[boundaries]

    # Define Lagrangian
    L = (u - u_MR)**2*dx + u*w*dx \
        + inner(mu*grad(u), grad(w))*dx \
        + alpha*(mu - mu_prior)**2*dx \
        - t_l*w*ds(0) - t_r*w*ds(1)
        
    # Compute directional derivatives
    F = derivative(L, y, z)

    # Solve nonlinear problem
    problem = NonlinearVariationalProblem(F, y, J=derivative(F, y))
    newtonsolver = NonlinearVariationalSolver(problem)
    max_iter = 40
    newtonsolver.parameters["newton_solver"]["maximum_iterations"] = max_iter
    newtonsolver.parameters["newton_solver"]["absolute_tolerance"] = 1E-13
    newtonsolver.parameters["newton_solver"]["relative_tolerance"] = 1E-12
    start = clock()
    try:
        iterations, convergence = newtonsolver.solve()
    except RuntimeError:
        iterations = max_iter
        convergence = False
    end = clock()
    print "finished solving"

    # Plot solution 
    if makeplot:
        plot(mesh)
        u_V = project(u, V)
        u_e_V = project(u_exact, V)
        u_MR_V = project(u_MR, V)
        mu_S = project(mu, S)
        w_V = project(w, V)
        plot(u_V, title="Numerical solution of u, N=%i, alpha=%s, noise=%s, mu_space=%s" % (N, alpha, noise, mu_space))
        plot(u_e_V, title="Analytical solution of u, N=%i, alpha=%s, noise=%s, mu_space=%s" % (N, alpha, noise, mu_space))
        plot(u_MR_V, title="u_MR, N=%i, alpha=%s, noise=%s, mu_space=%s" % (N, alpha, noise, mu_space))
        plot(mu_S, title="numerical solution of mu, N=%i, alpha=%s, noise=%s, mu_space=%s" % (N, alpha, noise, mu_space))
        plot(w_V, title="Numerical solution of w, N=%i, alpha=%s, noise=%s, mu_space=%s" % (N, alpha, noise, mu_space))
        interactive()

    if convergence:

        # Compute error in u
        (u, mu, w) = y.split(True)

        u_exact = Expression('exp(-k*x[0])', k = k, degree = degree + 4)
        mu_exact = Expression('1.0/(k*k)', k = k, degree = degree + 4)

        error_u_L2    = errornorm(u_exact, u, 'L2')
        error_u_H1    = errornorm(u_exact, u, 'H1')
        error_mu_L2   = errornorm(mu_exact, mu, 'L2')
        mu_approx_avg = numpy.mean(mu.vector().array())
        mu_std        = numpy.std(mu.vector().array())					# standard deviation in mu
        mu_e_vector   = numpy.ones(len(mu.vector().array()))*(1.0/k**2) # mu_exact = 1/k**2
        diff          = mu.vector().array() - numpy.ones(len(mu.vector().array()))*mu_approx_avg
        mu_dev        = numpy.abs(diff).max()							# max deviation in mu
    
        print "\nerror_u (L2-norm): ", error_u_L2
        print "error_u (H1-norm): ", error_u_H1
        print "mu_error (L2-norm): ", error_mu_L2
        print "mu average: %.4f" % mu_approx_avg
        print "mu std    : %.4f" % mu_std
        print "mu max dev: %.4f" % mu_dev

        return error_u_L2, error_u_H1, error_mu_L2, end - start, iterations, mu_approx_avg, mu_std, mu_dev
    else:
        return False, False, False, end - start, max_iter, False, False, False


N = 32
k = 2.0
noise = 0.0					# 0.0, 0.003, 0.01, 0.03, 0.1
degree = 1
makeplot = True			
alpha = 0.0
mu_space = "constant"		# "constant" or "DG"
mu_prior_function = "0"		# "0" or "mu_exact"
solver(N, k, noise, degree, makeplot, alpha, mu_space, mu_prior_function)
