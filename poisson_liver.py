from dolfin import *
import numpy, random

"""
This program solves the minimization problem
    ||u - u_MR||**2 + alpha*||mu - mu_prior||**2 --> min!
subject to 
    rho*omega*omega*u - div(mu * grad(u)) = 0  in Omega
                                 mu*du/dn = 0  on the boundary of Omega.
The domain Omega, is defined by [xmin, xmax]x[ymin, ymax], and u_MR in this domain is computed by the script readFile.py.
In these to scripts xmin, xmax, ymin and ymax must be the same.

Other boundary conditions are also implemented.

There are some different definitions of mu_prior.
"""

class MatFunc(Expression):
    def __init__(self, mat, factor):
        self.mat = mat
        self.factor = factor
    def eval(self, v, x): 
        i = int(x[0]*self.factor)
        j = int(x[1]*self.factor)
        # print i,j
        v[0] = self.mat[i,j]

# # Create classes for defining parts of the boundaries
# class Boundary(SubDomain):
#     def inside(self, x, on_boundary):
#         return on_boundary
# class Left(SubDomain):
#     def inside(self, x, on_boundary):
#         return near(x[0], 0.0)
# class Bottom(SubDomain):
#     def inside(self, x, on_boundary):
#         return near(x[1], 0.0)

# Define domain and resolution of MRE files
resolution = 1.5625e-3  # in meters
xmax = 155    #151
xmin = 90     #95
ymax = 190    #141
ymin = 150    #45
m = xmax - xmin         # Number of cells in x-direction
n = ymax - ymin         # Number of cells in y-direction
d = m*resolution        # x-max in mesh
e = n*resolution        # y-max in mesh

# # # Initialize sub-domain instances
# left = Left()
# bottom = Bottom()
# boundary = Boundary()

# Create mesh 
mesh = RectangleMesh(0, 0, d, e, m, n)

# # Initialize mesh function for boundary domains
# boundaries = FacetFunction("size_t", mesh)
# boundaries.set_all(0)
# left.mark(boundaries, 1)
# bottom.mark(boundaries, 2)

# Define function spaces and create mixed function space
V = FunctionSpace(mesh, 'Lagrange', 1)
# S = FunctionSpace(mesh, 'R', 0)
S = FunctionSpace(mesh, 'DG', 0)
# S = FunctionSpace(mesh, 'CG', 1)
M = MixedFunctionSpace([V, S, V])

# Define trial functions and test functions 
z = TestFunction(M)
(v, eta, q) = split(z)


# Define parameters
rho   = Constant(1000)      # density of water at body temp, kg/m**3
omega = 2*3.14*60           # angular frequency, f = 60 Hz
omega = Constant(omega)
a     = 1e-05
alpha = Constant(a)         # regularization parameter
displacement = numpy.load('disp_roi.npy') # in meters
# mu_calc = numpy.load('mu_roi.npy')

# Define u_MR
u_MR = MatFunc(displacement,((m-1)/d))

# # *******************************************************************************
# # ****************** Trying different definitions for mu_prior ******************
# Define mu_prior
mu_prior = Constant(2000)   # Pa

# # Test with mu_prior = random.randint(1700, 2300)
# def generate_mu(mesh):
#     mu_prior_function = MeshFunction("double", mesh, 2)
#     for cell in cells(mesh):
#         mu_prior_function[cell] = random.randint(1700, 2300)
#     mu_prior_function_file = File("mu_prior.xml.gz")
#     mu_prior_function_file << mu_prior_function
#     plot(mu_prior_function)
#     plot(mesh)
#     interactive()
# def read_mu(mesh):
#     # Code for C++ evaluation of mu_prior_function
#     mu_prior_code = """
#     class Mu_Prior : public Expression
#     {
#     public:
#       // Create expression
#       Mu_Prior() : Expression() {}
#       // Function for evaluating expression on each cell
#       void eval(Array<double>& values, const Array<double>& x, const ufc::cell& cell) const
#       {
#         const uint D = cell.topological_dimension;
#         const uint cell_index = cell.index;
#         values[0] = (*mu_prior_function)[cell_index];
#       }
#       // The data stored in mesh functions
#       boost::shared_ptr<MeshFunction<double> > mu_prior_function;
#     };
#     """
#     # Test with mu_prior = random.randint(1700, 2300)
#     mu_prior_function = MeshFunction("double", mesh, "mu_prior.xml.gz")
#     mu_function = Expression(cppcode=mu_prior_code)
#     mu_function.mu_prior_function = mu_prior_function
#     return mu_function
# # generate_mu(mesh)
# mu_prior = read_mu(mesh)

# # Test with mu_prior = (rho*omega*omega*u_MR)/(div(grad(u_MR)))
# mu_prior = MatFunc(mu_calc,((m-1)/d))

# # *******************************************************************************
# # *******************************************************************************

# Define function
y = Function(M)

# Set initial guesses
u0  = u_MR      # Expression("1.0")
mu0 = mu_prior  # Constant(2000) Expression("1.0")
w0  = Expression("0.0")

# Assign initial guesses to y
y0 = project(as_vector((u0, mu0, w0)), M)
y.assign(y0)

# Split mixed functions
(u, mu, w) = split(y)

# Define boundary conditions
# # Define new measures associated with the boundaries
# ds = Measure("ds")[boundaries]
# bcs = [DirichletBC(M.sub(0), u_MR, boundary)] 
# bcs = [DirichletBC(M.sub(0), u_MR, boundaries, 2)]
# n = FacetNormal(mesh)
# t = dot(grad(project(u_MR, V)), n)

# Define Lagrangian
L = (u - u_MR)**2*dx + rho*omega*omega*u*w*dx \
    + inner(mu*grad(u), grad(w))*dx \
    + alpha*(mu - mu_prior)**2*dx \
    # - mu_prior*t*w*ds
    
# Compute directional derivatives
F = derivative(L, y, z)

# Solve nonlinear problem
# problem = NonlinearVariationalProblem(F, y, bcs, J=derivative(F, y))
problem = NonlinearVariationalProblem(F, y, J=derivative(F, y))
newtonsolver = NonlinearVariationalSolver(problem)
# info(parameters, True)
newtonsolver.parameters["newton_solver"]["maximum_iterations"]   = 40
# newtonsolver.parameters["newton_solver"]["absolute_tolerance"]   = 1.0e-16
# newtonsolver.parameters["newton_solver"]["relative_tolerance"]   = 1.0e-15
# newtonsolver.parameters["newton_solver"]["relaxation_parameter"] = 0.5

try:
    iterations, convergence = newtonsolver.solve()
except RuntimeError:
    iterations = 40
    convergence = False
print "finished solving"

# Get sub-functions
u, mu, w = y.split()

# Plot solutions
plot(project(u_MR, V), title="u_MR")
plot(project(u, V), title="Numerical solution of u")
plot(project(mu, V), title="numerical solution of mu")
plot(project(w, V), title="Numerical solution of w")
plot(mesh, title="mesh")

# # Dump solution to file in VTK format
# u_file = File("u.pvd")
# u_file << u
# mu_file = File("mu.pvd")
# mu_file << mu
# w_file = File("w.pvd")
# w_file << w

interactive()



from dolfin import *
import numpy, random

"""
This program solves the minimization problem
    ||u - u_MR||**2 + alpha*||mu - mu_prior||**2 --> min!
subject to 
    rho*omega*omega*u - div(mu * grad(u)) = 0  in Omega
                                 mu*du/dn = 0  on the boundary of Omega.
The domain Omega, is defined by [xmin, xmax]x[ymin, ymax], and u_MR in this domain is computed by the script readFile.py.
In these to scripts xmin, xmax, ymin and ymax must be the same.

Other boundary conditions are also implemented.

There are some different definitions of mu_prior.
"""

class MatFunc(Expression):
    def __init__(self, mat, factor):
        self.mat = mat
        self.factor = factor
    def eval(self, v, x): 
        i = int(x[0]*self.factor)
        j = int(x[1]*self.factor)
        # print i,j
        v[0] = self.mat[i,j]

# # Create classes for defining parts of the boundaries
# class Boundary(SubDomain):
#     def inside(self, x, on_boundary):
#         return on_boundary
# class Left(SubDomain):
#     def inside(self, x, on_boundary):
#         return near(x[0], 0.0)
# class Bottom(SubDomain):
#     def inside(self, x, on_boundary):
#         return near(x[1], 0.0)

# Define domain and resolution of MRE files
resolution = 1.5625e-3  # in meters
xmax = 155    #151
xmin = 90     #95
ymax = 190    #141
ymin = 150    #45
m = xmax - xmin         # Number of cells in x-direction
n = ymax - ymin         # Number of cells in y-direction
d = m*resolution        # x-max in mesh
e = n*resolution        # y-max in mesh

# # # Initialize sub-domain instances
# left = Left()
# bottom = Bottom()
# boundary = Boundary()

# Create mesh 
mesh = RectangleMesh(0, 0, d, e, m, n)

# # Initialize mesh function for boundary domains
# boundaries = FacetFunction("size_t", mesh)
# boundaries.set_all(0)
# left.mark(boundaries, 1)
# bottom.mark(boundaries, 2)

# Define function spaces and create mixed function space
V = FunctionSpace(mesh, 'Lagrange', 1)
# S = FunctionSpace(mesh, 'R', 0)
S = FunctionSpace(mesh, 'DG', 0)
# S = FunctionSpace(mesh, 'CG', 1)
M = MixedFunctionSpace([V, S, V])

# Define trial functions and test functions 
z = TestFunction(M)
(v, eta, q) = split(z)


# Define parameters
rho   = Constant(1000)      # density of water at body temp, kg/m**3
omega = 2*3.14*60           # angular frequency, f = 60 Hz
omega = Constant(omega)
a     = 1e-05
alpha = Constant(a)         # regularization parameter
displacement = numpy.load('disp_roi.npy') # in meters
# mu_calc = numpy.load('mu_roi.npy')

# Define u_MR
u_MR = MatFunc(displacement,((m-1)/d))

# # *******************************************************************************
# # ****************** Trying different definitions for mu_prior ******************
# Define mu_prior
mu_prior = Constant(2000)   # Pa

# # Test with mu_prior = random.randint(1700, 2300)
# def generate_mu(mesh):
#     mu_prior_function = MeshFunction("double", mesh, 2)
#     for cell in cells(mesh):
#         mu_prior_function[cell] = random.randint(1700, 2300)
#     mu_prior_function_file = File("mu_prior.xml.gz")
#     mu_prior_function_file << mu_prior_function
#     plot(mu_prior_function)
#     plot(mesh)
#     interactive()
# def read_mu(mesh):
#     # Code for C++ evaluation of mu_prior_function
#     mu_prior_code = """
#     class Mu_Prior : public Expression
#     {
#     public:
#       // Create expression
#       Mu_Prior() : Expression() {}
#       // Function for evaluating expression on each cell
#       void eval(Array<double>& values, const Array<double>& x, const ufc::cell& cell) const
#       {
#         const uint D = cell.topological_dimension;
#         const uint cell_index = cell.index;
#         values[0] = (*mu_prior_function)[cell_index];
#       }
#       // The data stored in mesh functions
#       boost::shared_ptr<MeshFunction<double> > mu_prior_function;
#     };
#     """
#     # Test with mu_prior = random.randint(1700, 2300)
#     mu_prior_function = MeshFunction("double", mesh, "mu_prior.xml.gz")
#     mu_function = Expression(cppcode=mu_prior_code)
#     mu_function.mu_prior_function = mu_prior_function
#     return mu_function
# # generate_mu(mesh)
# mu_prior = read_mu(mesh)

# # Test with mu_prior = (rho*omega*omega*u_MR)/(div(grad(u_MR)))
# mu_prior = MatFunc(mu_calc,((m-1)/d))

# # *******************************************************************************
# # *******************************************************************************

# Define function
y = Function(M)

# Set initial guesses
u0  = u_MR      # Expression("1.0")
mu0 = mu_prior  # Constant(2000) Expression("1.0")
w0  = Expression("0.0")

# Assign initial guesses to y
y0 = project(as_vector((u0, mu0, w0)), M)
y.assign(y0)

# Split mixed functions
(u, mu, w) = split(y)

# Define boundary conditions
# # Define new measures associated with the boundaries
# ds = Measure("ds")[boundaries]
# bcs = [DirichletBC(M.sub(0), u_MR, boundary)] 
# bcs = [DirichletBC(M.sub(0), u_MR, boundaries, 2)]
# n = FacetNormal(mesh)
# t = dot(grad(project(u_MR, V)), n)

# Define Lagrangian
L = (u - u_MR)**2*dx + rho*omega*omega*u*w*dx \
    + inner(mu*grad(u), grad(w))*dx \
    + alpha*(mu - mu_prior)**2*dx \
    # - mu_prior*t*w*ds
    
# Compute directional derivatives
F = derivative(L, y, z)

# Solve nonlinear problem
# problem = NonlinearVariationalProblem(F, y, bcs, J=derivative(F, y))
problem = NonlinearVariationalProblem(F, y, J=derivative(F, y))
newtonsolver = NonlinearVariationalSolver(problem)
# info(parameters, True)
newtonsolver.parameters["newton_solver"]["maximum_iterations"]   = 40
# newtonsolver.parameters["newton_solver"]["absolute_tolerance"]   = 1.0e-16
# newtonsolver.parameters["newton_solver"]["relative_tolerance"]   = 1.0e-15
# newtonsolver.parameters["newton_solver"]["relaxation_parameter"] = 0.5

try:
    iterations, convergence = newtonsolver.solve()
except RuntimeError:
    iterations = 40
    convergence = False
print "finished solving"

# Get sub-functions
u, mu, w = y.split()

# Plot solutions
plot(project(u_MR, V), title="u_MR")
plot(project(u, V), title="Numerical solution of u")
plot(project(mu, V), title="numerical solution of mu")
plot(project(w, V), title="Numerical solution of w")
plot(mesh, title="mesh")

# # Dump solution to file in VTK format
# u_file = File("u.pvd")
# u_file << u
# mu_file = File("mu.pvd")
# mu_file << mu
# w_file = File("w.pvd")
# w_file << w

interactive()




