from dolfin import *
import random


def forward_solver(N, makeplot=False):
    # Define mesh and function space
    mesh = UnitSquareMesh(N, N)
    V = VectorFunctionSpace(mesh, "Lagrange", 3)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)

    # Define the Lame parameters and the traction force
    mu    = 3.0
    lmbda = 8.0 
    t = Constant((0.9, 0.3))

    def sigma(v):
        return 2.0*mu*sym(grad(v)) + lmbda*tr(sym(grad(v)))*Identity(v.cell().d)

    # Define the variational problem
    a = inner(u, v)*dx + inner(sigma(u), grad(v))*dx
    L = dot(t, v)*ds

    # Compute solution
    u = Function(V)
    solve(a == L, u)

    if makeplot:
        # Plot solution
        plot(u, interactive=True, mode="displacement")

    return u


N_list = [4, 8, 16, 32, 64, 128]
for N in N_list:
	u_exact = forward_solver(N)
	for noise in [0.1, 0.03, 0.01, 0.003, 0.0]:
		print "Creating noise, N=%i, noise=%s" % (N, noise)
		mesh = UnitSquareMesh(N, N)

		u_MRnoise = MeshFunction("double", mesh, 2)

		for cell in cells(mesh):
			u_MRnoise[cell] = random.gauss(0, noise)*norm(u_exact, 'L2')

		u_MRnoise_file = File("noise_elasticity/u_MR_noise_%s_N_%i.xml.gz"% (noise, N))

		u_MRnoise_file << u_MRnoise

		plot(u_MRnoise, title="noise_%s_N_%i" % (noise, N))
		# interactive()
print "finished"