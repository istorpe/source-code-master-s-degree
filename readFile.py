
import numpy
from dolfin import *
import csv
from math import pi
import random

"""
This program computes the displacement and shear modulus in an MRE image and stores the solutions to the files
disp_roi.npy and mu_roi.npy, respectively.

The region of interest (roi) is defined by xmin, xmax, ymin and ymax.
"""

class MatFunc(Expression):
    def __init__(self, mat, factor):
        self.mat = mat
        self.factor = factor
    def eval(self, v, x): 
        i = int(x[0]*self.factor)
        j = int(x[1]*self.factor)
        # print i,j
        v[0] = self.mat[i,j]

N = 256 
filename = "deformationFiles/testInterp2.csv"
f = open(filename)
reader = csv.reader(f)

lines = []

count = 0
for row in reader:
    if count < N:
        test = [float(col) for col in row] 
        lines.append(test)
        count += 1

a = numpy.array(lines)

menc = 20*10**(-3)				# parameter from elastography
phaserange = pi*1000			# parameter from elastography
disp = a*menc*2*pi/phaserange

resolution = 1.5625e-3
xmax = 155    #151
xmin = 90     #95
ymax = 190    #141
ymin = 150    #45
m = xmax - xmin
n = ymax - ymin
d = m*resolution#0.4
e = n*resolution

# Define parameters
rho   = Constant(1000)      # density of water at body temp, kg/m**3
omega = 2*3.14*60           # angular frequency, 60 Hz
omega = Constant(omega)

mesh = RectangleMesh(0, 0, d, e, m, n)
V = FunctionSpace(mesh, "Lagrange", 1)

u_MR_wholeimage = numpy.zeros([N,N])  
for i in range(N):  
    for j in range(N): 
        u_MR_wholeimage[i,j] = disp[i,j]#numpy.sin(20.0*i*j/(N*N))  

u_MR_roi = u_MR_wholeimage[xmin:xmax, ymin:ymax]
numpy.save('disp_roi.npy', u_MR_roi) # save region of interest
print "file disp_roi.npy saved"

u_MR_roi_vector = u_MR_roi.reshape(m*n,)
u_MR_Expression = MatFunc(u_MR_roi, ((m-1)/d)) 
u_MR_projection = project(u_MR_Expression, V)
u_MR_vector     = u_MR_projection.vector()

mesh2 = UnitSquareMesh(m-1, n-1)
W = FunctionSpace(mesh2, "Lagrange", 1)
u = TrialFunction(W)
v = TestFunction(W)
M = assemble(rho*omega*omega*u*v*dx)
A = assemble(inner(nabla_grad(u), nabla_grad(v))*dx)
Mu = M*u_MR_roi_vector
Au = A*u_MR_roi_vector
mu_roi_vector = numpy.ndarray(shape=(m*n, ))

for i in range(len(mu_roi_vector)):
    # try:
    if Mu[i] == 0.0 or Au[i] == 0.0:
    	mu_roi_vector[i] = mu_roi_vector[i-1]
    else:
	    mu_roi_vector[i] = Mu[i]/Au[i]
    # except RuntimeWarning:
    # 	try:
    #     	mu_roi_vector[i] = mu_roi_vector[i-1]
    #     except:
    #     	mu_roi_vector[i] = 2000
print "mu", mu_roi_vector
# for i in range(len(mu_roi_vector)):
# 	if mu_roi_vector[i] <= 0.0:
# 		mu_roi_vector[i] = -mu_roi_vector[i]
# print mu_roi_vector

mu = mu_roi_vector.reshape(m, n)
numpy.save('mu_roi.npy', mu)
print "file mu_roi.npy saved"

mu_Expression = MatFunc(mu, ((m-1)/d)) 
mu_projection = project(mu_Expression, V)
mu_vector     = mu_projection.vector()
print "mu_vector", mu_vector.array()

plot(u_MR_projection, title="u_MR")
plot(mu_projection, title="mu")
interactive()

# FunctionSpace(mesh, "DG", 0)
# test = MeshFunction("double", mesh, 2)
# print mesh.num_cells()
# for cell in cells(mesh):
#     test[cell] = random.randint(1700, 2300)

# muprior_file = File("mu_prior.xml.gz")

# muprior_file << test
