from dolfin import *
import ufl, sys, random, numpy
from time import clock, strftime, gmtime

"""
Solves minimization problem 
    ||u - u_MR||**2 + alpha*||mu - mu_prior||**2 --> min!
subject to
    u(x, y) - div sigma = 0
                sigma n = t
"""


def convergence_rate(E, h):
    from math import log as ln
    r = []
    for i in range(1, len(E)):
        if E[i] and E[i-1]:
            r.append(ln(E[i]/E[i-1])/ln(h[i]/h[i-1]))
        else:
            r.append("False")
    return r

def forward_solver(N, makeplot=False):
    # Define mesh and function space
    mesh = UnitSquareMesh(N, N)
    V = VectorFunctionSpace(mesh, "Lagrange", 3)

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)

    # Define the Lame parameters and the traction force
    mu    = 3.0
    lmbda = 8.0 
    t = Constant((0.9, 0.3))

    def sigma(v):
        return 2.0*mu*sym(grad(v)) + lmbda*tr(sym(grad(v)))*Identity(v.cell().d)

    # Define the variational problem
    a = inner(u, v)*dx + inner(sigma(u), grad(v))*dx
    L = dot(t, v)*ds

    # Compute solution
    u = Function(V)
    solve(a == L, u)

    if makeplot:
        # Plot solution
        plot(u, interactive=True, mode="displacement")

    return u, mu, lmbda, t

def solver(N, u_exact, mu_e, lmbda_e, traction, noise, degree=1, makeplot=False, alpha=0.0, beta=0.0, prm_space="constant", mu_prior_function="0", lmbda_prior_function="0"):
    # Create mesh
    mesh = UnitSquareMesh(N, N)

    # Define function space and mixed product space
    V = VectorFunctionSpace(mesh, 'Lagrange', degree)
    if prm_space == "constant":
        S = FunctionSpace(mesh, 'R', 0)                 
    elif prm_space == "DG":
        S = FunctionSpace(mesh, 'DG', 0)                
    else:
        print "prm_space must be either 'constant' or 'DG'."
        exit()

    M = MixedFunctionSpace([V, S, S, V])

    # Code for C++ evaluation of noise function
    noise_code = """
    class Noise : public Expression
    {
    public:
      // Create expression with 2 components
      Noise() : Expression(2) {}
      // Function for evaluating expression on each cell
      void eval(Array<double>& values, const Array<double>& x, const ufc::cell& cell) const
      {
        const uint D = cell.topological_dimension;
        const uint cell_index = cell.index;
        values[0] = (*noise_mesh)[cell_index];
        values[1] = (*noise_mesh)[cell_index];
      }
      // The data stored in mesh functions
      boost::shared_ptr<MeshFunction<double> > noise_mesh;
    };
    """

    mu_exact = Expression(("mu_exact"), mu_exact=mu_e, degree=degree)
    lmbda_exact =  Expression(("lmbda_exact"), lmbda_exact=lmbda_e, degree=degree)

    if noise != 0.0:
        # Define noise expression 
        noise_mesh = MeshFunction("double", mesh, "noise_elasticity/u_MR_noise_%s_N_%i.xml.gz" % (noise, N))
        noise_function = Expression(cppcode=noise_code)
        noise_function.noise_mesh = noise_mesh
        u_MR = u_exact + noise_function
    else:
        u_MR = u_exact

    if mu_prior_function == "0":
        mu_prior = Constant(0.0)
    elif mu_prior_function == "mu_exact":
        mu_prior = mu_exact
    if lmbda_prior_function == "0":
        lmbda_prior = Constant(0.0)
    elif lmbda_prior_function == "lmbda_exact":
        lmbda_prior = lmbda_exact

    print "----------------------------------------------------------------------------------"
    print "Computing solution. mu_prior = %s, lmbda_prior = %s, alpha = %s, beta = %s, noise = %s, N = %i, degree = %i \n" \
            % (mu_prior_function, lmbda_prior_function, alpha, beta, noise, N, degree)

    # Define trial functions and test functions 
    z = TestFunction(M)
    (v, eta, kappa, q) = split(z)

    # Define function
    y = Function(M)

    # Set initial guesses
    u0 = u_exact
    mu0 = mu_exact
    lmbda0 = lmbda_exact 
    w0 = Expression(("1.0", "1.0"))

    y0 = project(as_vector((u0[0], u0[1], mu0, lmbda0, w0[0], w0[1])), M)
    y.assign(y0)

    # Split mixed functions
    (u, mu, lmbda, w) = split(y) 

    def  epsilon(u): 
        """ Strain tensor """ 
        return 0.5*(grad(u) + transpose(grad(u))) 

    def sigma(v, mu, lmbda): 
        """ Stress tensor """ 
        return 2.0*mu*sym(grad(v)) + lmbda*tr(sym(grad(v)))*Identity(v.cell().d)

    L = (u - u_MR)**2*dx \
        + dot(u, w)*dx \
        + inner(sigma(u, mu, lmbda), grad(w))*dx \
        + alpha*(mu - mu_prior)**2*dx \
        + beta*(lmbda - lmbda_prior)**2*dx \
        - dot(traction, w)*ds

    # Compute directional derivatives
    F = derivative(L, y, z)

    # Solve nonlinear problem
    problem = NonlinearVariationalProblem(F, y, J=derivative(F, y))
    newtonsolver = NonlinearVariationalSolver(problem)
    max_iter = 40
    newtonsolver.parameters["newton_solver"]["maximum_iterations"] = max_iter
    # newtonsolver.parameters["newton_solver"]["absolute_tolerance"] = 1E-13
    # newtonsolver.parameters["newton_solver"]["relative_tolerance"] = 1E-12
    start = clock()
    try:
        iterations, convergence = newtonsolver.solve()
    except RuntimeError:
        iterations = max_iter
        convergence = False
    end = clock()

    print "finished solving"

    # Plot solution
    if makeplot:
        u_V     = project(u, V)
        u_e_V   = project(u_exact, V)
        u_MR_V  = project(u_MR, V)
        mu_S    = project(mu, S)
        lmbda_S = project(lmbda, S)
        w_V     = project(w, V)
        plot(u_V, mode="displacement",title="Numerical solution of u")
        plot(u_e_V, mode="displacement", title="Analytical solution of u")
        plot(u_MR, mode="displacement" ,title="u_MR")
        plot(mu_S, title="Numerical solution of mu")
        plot(lmbda_S, title="Numerical solution of lambda")
        plot(w_V, mode="displacement", title="Numerical solution of w")
        interactive()

    if convergence:
        (u, mu, lmbda, w) = y.split(True)

        # Compute error in u, mu, lmbda
        error_u_L2, error_u_H1      = errornorm(u_exact, u, 'L2'),errornorm(u_exact, u, 'H1')
        error_mu_L2, error_lmbda_L2 = errornorm(mu_exact, mu, 'L2'), errornorm(lmbda_exact, lmbda, 'L2')
        mu_S, lmbda_S     = project(mu, S), project(lmbda, S)
        mu_e_S, lmbda_e_S = project(mu_exact, S), project(lmbda_exact, S)
        mu_approx_avg     = numpy.mean(mu.vector().array())
        mu_std            = numpy.std(mu.vector().array())
        mu_e_vector       = numpy.ones(len(mu.vector().array()))*mu_e
        diff_mu           = mu.vector().array() - numpy.ones(len(mu.vector().array()))*mu_approx_avg
        mu_dev            = numpy.absolute(diff_mu).max()
        lmbda_approx_avg  = numpy.mean(lmbda.vector().array())
        lmbda_std         = numpy.std(lmbda.vector().array())
        lmbda_e_vector    = numpy.ones(len(lmbda.vector().array()))*lmbda_e
        diff_lmbda        = lmbda.vector().array() - numpy.ones(len(lmbda.vector().array()))*lmbda_approx_avg
        lmbda_dev         = numpy.absolute(diff_lmbda).max()

        print "u_error_L2: %.3E, u_error_H1: %.3E" % (error_u_L2, error_u_H1)
        if prm_space == "constant":
            print "mu_exact: %8.3f, mu_approx: %8.3f, mu_error: %8.3f" \
                % (mu_e_S.vector().array()[0], mu.vector().array()[0], error_mu_L2)
            print "lmbda_exact: %5.3f, lmbda_approx: %5.3f, lmbda_error: %5.3f" \
                % (lmbda_e_S.vector().array()[0], lmbda.vector().array()[0], error_lmbda_L2)
        elif prm_space == "DG":
            print "mu_exact: %8.3f, mu_avg: %8.3f, mu_error: %8.3f, mu_std: %8.3f, mu_dev: %8.3f" \
                % (mu_e_S.vector().array()[0], mu_approx_avg, error_mu_L2, mu_std, mu_dev)
            print "lmbda_exact: %5.3f, lmbda_avg: %5.3f, lmbda_error: %5.3f, lmbda_std: %5.3f, lmbda_dev: %5.3f" \
                % (lmbda_e_S.vector().array()[0], lmbda_approx_avg, error_lmbda_L2, lmbda_std, lmbda_dev)

        return error_u_L2, error_u_H1, error_mu_L2, error_lmbda_L2, end - start, iterations, \
                mu_approx_avg, lmbda_approx_avg, mu_std, lmbda_std, mu_dev, lmbda_dev
    else:
        return False, False, False, False, end - start, max_iter, False, False, False, False, False, False

N = 32
noise = 0.0                 # 0.0, 0.003, 0.01, 0.03, 0.1
degree = 1
makeplot = True
alpha = 0.0
beta = 0.0
prm_space = "constant"      # "constant" or "DG"
mu_prior_function = "0"     # "0" or "mu_exact"
lmbda_prior_function = "0"  # "0" or "lmbda_exact"

u, mu, lmbda, t = forward_solver(N, False)
solver(N, u, mu, lmbda, t, noise, degree, makeplot, alpha, beta, prm_space, mu_prior_function, lmbda_prior_function)